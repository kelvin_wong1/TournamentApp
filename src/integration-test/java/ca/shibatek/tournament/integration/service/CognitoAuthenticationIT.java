package ca.shibatek.tournament.integration.service;

import ca.shibatek.tournament.authentication.CognitoIdentityProvider;
import ca.shibatek.tournament.dto.UserDto;
import ca.shibatek.tournament.service.CognitoAuthenticationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CognitoIdentityProvider.class, CognitoAuthenticationService.class})
public class CognitoAuthenticationIT {

    @Autowired
    private CognitoAuthenticationService cognitoAuthenticationService;

    @Test
    public void shouldAddUserToCognitoService() throws Exception{
        UserDto userDto = new UserDto();
        userDto.setEmail("erika_kitagawa_1@yahoo.com");
        userDto.setFirstName("Erika");
        userDto.setLastName("Kitagawa");
        userDto.setPassword("Password1!");

        cognitoAuthenticationService.registerUser(userDto);
    }

}
