package ca.shibatek.tournament.authentication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;


@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CognitoIdentityProvider {

    private CognitoIdentityProviderClient cognitoIdentityProviderClient;
    @Value("${aws.profile}")
    private String awsProfileName;
    @Value("${aws.clientId}")
    private String awsClientId;
    @Value("${aws.clientSecret}")
    private String awsClientSecret;
    @Value("${aws.userPoolId}")
    private String awsUserPoolId;

    public CognitoIdentityProviderClient getCognitoIdentityProviderClient(){
        if(cognitoIdentityProviderClient == null){

            ProfileCredentialsProvider profileCredentialsProvider =
                    ProfileCredentialsProvider.builder().profileName(awsProfileName).build();

            cognitoIdentityProviderClient = CognitoIdentityProviderClient.builder()
                    .credentialsProvider(profileCredentialsProvider)
                    .build();
        }
        return cognitoIdentityProviderClient;
    }

    public String getAwsClientId(){
        return this.awsClientId;
    }

    public String getAwsClientSecret(){
        return this.awsClientSecret;
    }
}
