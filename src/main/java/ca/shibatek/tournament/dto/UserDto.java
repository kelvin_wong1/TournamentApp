package ca.shibatek.tournament.dto;

import ca.shibatek.tournament.constraint.FieldMatch;
import ca.shibatek.tournament.constraint.ValidEmail;
import ca.shibatek.tournament.constraint.ValidPassword;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@FieldMatch(first = "password", second = "passwordConfirmation", message = "Passwords must match")
public class UserDto {

    @NotNull(message = "Required")
    @NotEmpty(message = "Required")
    private String firstName;
    @NotNull(message = "Required")
    @NotEmpty(message = "Required")
    private String lastName;
    @ValidEmail
    private String email;
    @ValidPassword
    private String password;
    private String passwordConfirmation;
    private boolean isOrganizer = false;

    @Override
    public String toString(){
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("User DTO[firstname=").append(firstName).append("; lastName=").append(lastName)
                .append("; email=").append(email).append("]");

        return stringBuilder.toString();
    }
}
