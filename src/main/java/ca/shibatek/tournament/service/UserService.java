package ca.shibatek.tournament.service;

import ca.shibatek.tournament.dto.UserDto;
import ca.shibatek.tournament.model.User;

public interface UserService {

    User registerUser(UserDto userDto) throws RegistrationException;
}
