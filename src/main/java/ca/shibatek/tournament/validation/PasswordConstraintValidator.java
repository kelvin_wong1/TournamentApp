package ca.shibatek.tournament.validation;

import ca.shibatek.tournament.constraint.ValidPassword;
import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public void initialize(ValidPassword arg0){

    }

    @Override
    public boolean isValid(final String password, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(
                new LengthRule(8,30),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Special, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1)
        );

        RuleResult result = validator.validate(new PasswordData(password));

        if(result.isValid()){
            return true;
        }

        String errorMessage = getErrorMessage(validator.getMessages(result));
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errorMessage)
                .addConstraintViolation();
        return false;
    }

    private String getErrorMessage(List<String> errorMessages){
        StringBuilder stringBuilder = new StringBuilder();
        for(String message : errorMessages){
            stringBuilder.append(message);
            stringBuilder.append("<br>");
        }

        return stringBuilder.toString();
    }
}
