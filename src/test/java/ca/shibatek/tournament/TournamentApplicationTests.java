package ca.shibatek.tournament;

import ca.shibatek.tournament.controller.RegistrationController;
import ca.shibatek.tournament.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TournamentApplicationTests {

	@Autowired
	private RegistrationController registrationController;
	@Autowired
	private UserService userService;

	@Test
	@DisplayName("Test Spring context loads")
	public void contextLoads() {
		assertThat(registrationController).isNotNull();
		assertThat(userService).isNotNull();
	}

}
