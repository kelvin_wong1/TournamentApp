package ca.shibatek.tournament.util;

import ca.shibatek.tournament.dto.UserDto;

public class TestUtil {

    public static UserDto getUserDto(){
        UserDto userDto = new UserDto();
        userDto.setEmail("test@test.com");
        userDto.setFirstName("Anna");
        userDto.setLastName("Kendrick");

        return userDto;
    }
}
