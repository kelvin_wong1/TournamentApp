package ca.shibatek.tournament.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class EmailValidatorTest {

    private EmailValidator emailValidator;

    @BeforeEach
    public void setUp(){
        this.emailValidator = new EmailValidator();
    }

    @ParameterizedTest
    @ValueSource(strings = {"anna.kendrick@test.com", "anna_kendrick@test.co.ca", "anna-kendrick@test.net",
            "anna@test.com", "ms_kendrick@the-prettiest.com"})
    public void shouldReturnTrueForValidEmail(final String email){
        assertTrue(emailValidator.isValid(email, null));
    }

    @ParameterizedTest
    @ValueSource(strings = {"masami.ichikawa@sod", "masami@domain-com"})
    public void shouldReturnFalseForInvalidEmail(final String email){
        assertFalse(emailValidator.isValid(email, null));
    }
}