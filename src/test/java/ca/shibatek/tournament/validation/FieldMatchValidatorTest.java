package ca.shibatek.tournament.validation;

import ca.shibatek.tournament.util.TestUtil;
import ca.shibatek.tournament.dto.UserDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FieldMatchValidatorTest {

    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldHaveNoErrorsWhenPasswordsMatch() {
        UserDto userDto = TestUtil.getUserDto();
        userDto.setPassword("P@ssword!2");
        userDto.setPasswordConfirmation("P@ssword!2");

        Set<ConstraintViolation<UserDto>> constraintViolations = validator.validate(userDto);

        assertEquals(constraintViolations.size(), 0);
    }

    @Test
    public void shouldHaveErrorWhenPasswordsDoNotMatch() {
        UserDto userDto = TestUtil.getUserDto();
        userDto.setPassword("P@ssword!2");
        userDto.setPasswordConfirmation("invalid-password");

        Set<ConstraintViolation<UserDto>> constraintViolations = validator.validate(userDto);

        assertEquals(constraintViolations.size(), 1);
    }
}
