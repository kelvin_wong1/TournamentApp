package ca.shibatek.tournament.validation;

import ca.shibatek.tournament.util.TestUtil;
import ca.shibatek.tournament.dto.UserDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PasswordConstraintValidatorTest {

    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @ParameterizedTest
    @ValueSource(strings = {"P@ssword!2", "P@SSWORd!!2", "@ttff6SE", "p@SSword_2"})
    public void shouldHaveNoValidationErrorsWithValidPassword(final String password){
        UserDto userDto = TestUtil.getUserDto();
        userDto.setPassword(password);
        userDto.setPasswordConfirmation(password);

        Set<ConstraintViolation<UserDto>> constraintViolations = validator.validate(userDto);

        assertEquals(constraintViolations.size(), 0);
    }

    @ParameterizedTest
    @ValueSource(strings = {"2short", "nocaps!1", "NOLOWERCASE_1", "NoSpecial2", "NoNumber!"})
    public void shouldHaveValidationErrorsWithInvalidPasswords(final String password){
        UserDto userDto = TestUtil.getUserDto();
        userDto.setPassword(password);
        userDto.setPasswordConfirmation(password);

        Set<ConstraintViolation<UserDto>> constraintViolations = validator.validate(userDto);

        assertEquals(constraintViolations.size(), 1);
    }
}
